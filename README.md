# plf08

## Descripción
Aplicación que lleva a cabo el cifrado y descifrado mediante el Cuadrado de Polibio y Transposición columnar simple. 

## Forma de ejecutar
Para ejecutar la aplicación se hace uso de la consola siguiendo las siguientes instrucciones:  
1. Ubicar en la consola la ruta de la carpeta de la aplicación.
2. El comando para ejecutar la aplicación es lein run *parametros*
    - Los parámetros son los siguientes
        + Tipo de cifrado (1=Polibio, 0=Transposicion)
        + Operación a realizar (1=Cifrado, 0=Descifrado)
        + Palabra clave (para el cifrado de Transposición)
        + Archivo de texto plano a leer
        + Archivo de texto plano a escribir
3. Presionar Enter y visualizar el resultado.

## Opciones de ejecución
Cifrar un texto con Polibio

    lein run 1 1 "archivo-a-leer.txt" "archivo-a-escribir.txt"

Descifrar un texto con Polibio

    lein run 1 0 "archivo-a-leer.txt" "archivo-a-escribir.txt"

Cifrar un texto con Transposición columnar simple.  

    lein run 0 1 "clave" "archivo-a-leer.txt" "archivo-a-escribir.txt"

Descifrar un texto con Transposición columnar simple.

    lein run 0 0 "clave" "archivo-a-leer.txt" "archivo-a-escribir.txt"

## Ejemplos de uso o ejecución
### Cifrado de Polibio
![cifrado](/uploads/6aa738eae8d04771cd9dc0b562401147/cifrado.png)

### Descifrado de Polibio
![descifrado](/uploads/ebb03a50bafdd9fa1d556fde23b83bb0/descifrado.jpg)

### Cifrado de Transposición columnar simple
![cifrado-transposicion](/uploads/7aa5629d46d5a5d62e7d9babb0a77ed4/cifrado-transposicion.jpg)

### Descifrado de Transposición columnar simple
![descifrado-transposicion](/uploads/6e5f9b70b2422af2da950eb84850bd93/descifrado-transposicion.jpg)

## Errores, limitantes, situaciones no consideradas o particularidades de tu solución
1. Cifrado de Polibio
    - Los últimos espacios que quedaron en el cuadro de Polibio se utilizaron para las letras mayusculas, pero faltaron espacios para las letras Y y Z.
    - Para los carácteres desconocidos se respetaron pero al mismo tiempo se duplicaban para no tener problemas al momento de descifrar, es decir para tener un número par de carácteres.  
2. Cifrado por Transposición columnar simple.
    - Para procesar las palabras que no completan el rectangulo formado por la palabra clave, los cuadros restantes se rellenan con espacios en blanco.
    - Al descifrar una palabra es probable que el resultado muestra la palabra original pero con algunos espacios al final, esto a causa del punto anterior.
