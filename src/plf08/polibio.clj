(ns plf08.polibio)

(defn tabla-conversion
  []
  (vector [\a "♜♜"]
          [\á "♜♞"]
          [\b "♜♝"]
          [\c "♜♛"]
          [\d "♜♚"]
          [\e "♜♖"]
          [\é "♜♘"]
          [\f "♜♗"]
          [\g "♜♕"]
          [\h "♜♔"]
          [\i "♞♜"]
          [\í "♞♞"]
          [\j "♞♝"]
          [\k "♞♛"]
          [\l "♞♚"]
          [\m "♞♖"]
          [\n "♞♘"]
          [\ñ "♞♗"]
          [\o "♞♕"]
          [\ó "♞♔"]
          [\p "♝♜"]
          [\q "♝♞"]
          [\r "♝♝"]
          [\s "♝♛"]
          [\t "♝♚"]
          [\u "♝♖"]
          [\ú "♝♘"]
          [\ü "♝♗"]
          [\v "♝♕"]
          [\w "♝♔"]
          [\x "♛♜"]
          [\y "♛♞"]
          [\z "♛♝"]
          [\0 "♛♛"]
          [\1 "♛♚"]
          [\2 "♛♖"]
          [\3 "♛♘"]
          [\4 "♛♗"]
          [\! "♛♕"]
          [\" "♛♔"]
          [\# "♚♜"]
          [\$ "♚♞"]
          [\% "♚♝"]
          [\& "♚♛"]
          [\' "♚♚"]
          [\( "♚♖"]
          [\) "♚♘"]
          [\* "♚♗"]
          [\+ "♚♕"]
          [\, "♚♔"]
          [\- "♖♜"]
          [\. "♖♞"]
          [\/ "♖♝"]
          [\: "♖♛"]
          [\; "♖♚"]
          [\< "♖♖"]
          [\= "♖♘"]
          [\> "♖♗"]
          [\? "♖♕"]
          [\@ "♖♔"]
          [\[ "♘♜"]
          [\\ "♘♞"]
          [\] "♘♝"]
          [\^ "♘♛"]
          [\_ "♘♚"]
          [\` "♘♖"]
          [\{ "♘♘"]
          [\| "♘♗"]
          [\} "♘♕"]
          [\~ "♘♔"]
          [\5 "♗♜"]
          [\6 "♗♞"]
          [\7 "♗♝"]
          [\8 "♗♛"]
          [\9 "♗♚"]
          [\A "♗♖"]
          [\B "♗♘"]
          [\C "♗♗"]
          [\D "♗♕"]
          [\E "♗♔"]
          [\F "♕♜"]
          [\G "♕♞"]
          [\H "♕♝"]
          [\I "♕♛"]
          [\J "♕♚"]
          [\K "♕♖"]
          [\L "♕♘"]
          [\M "♕♗"]
          [\N "♕♕"]
          [\Ñ "♕♔"]
          [\O "♔♜"]
          [\P "♔♞"]
          [\Q "♔♝"]
          [\R "♔♛"]
          [\S "♔♚"]
          [\T "♔♖"]
          [\U "♔♘"]
          [\V "♔♗"]
          [\W "♔♕"]
          [\X "♔♔"]))

(defn p-cifrar
  [xs]
  (let [tabla (tabla-conversion)
        filtro (fn [c] (filter (fn [v] (= (first v) c)) tabla))
        convertir (fn [c] (let [vt (flatten (filtro c))]
                            (if (empty? vt) (str c c) (last vt))))]
    (apply str (map (fn [c] (convertir c)) xs))))

;; KIBIT
;; (defn p-cifrar
;;   [xs]
;;   (let [tabla (tabla-conversion)
;;         filtro (fn [c] (filter (fn [v] (= (first v) c)) tabla))
;;         convertir (fn [c] (let [vt (flatten (filtro c))]
;;                             (if (empty? vt) (str c c) (last vt))))]
;;     (clojure.string/join (map convertir xs))))


(defn p-descifrar
  [xs]
  (let [tabla (tabla-conversion)
        filtro (fn [v] (filter (fn [ys] (= (last ys) v)) tabla))
        convertir (fn [v] (let [vt (flatten (filtro v))]
                            (if (empty? vt) (str (first v)) (first vt))))]
    (apply str (map (fn [v] (convertir v)) (map (fn [v] (apply str v)) (partition-all 2 xs))))))

;;KIBIT
;; (defn p-descifrar
;;   [xs]
;;   (let [tabla (tabla-conversion)
;;         filtro (fn [v] (filter (fn [ys] (= (last ys) v)) tabla))
;;         convertir (fn [v] (let [vt (flatten (filtro v))]
;;                             (if (empty? vt) (str (first v)) (first vt))))]
;;     (clojure.string/join (map convertir (map (fn [v] (clojure.string/join v)) (partition-all 2 xs))))))
