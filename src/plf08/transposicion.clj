(ns plf08.transposicion)

(defn tabla-transposicion
  []
  (hash-map \a 1
            \A 1
            \á 2
            \Á 2
            \b 3
            \B 3
            \c 4
            \C 4
            \d 5
            \D 5
            \e 6
            \E 6
            \é 7
            \É 7
            \f 8
            \F 8
            \g 9
            \G 9
            \h 10
            \H 10
            \i 11
            \I 11
            \í 12
            \Í 12
            \j 13
            \J 13
            \k 14
            \K 14
            \l 15
            \L 15
            \m 16
            \M 16
            \n 17
            \N 17
            \ñ 18
            \Ñ 18
            \o 19
            \O 19
            \ó 20
            \Ó 20
            \p 21
            \P 21
            \q 22
            \Q 22
            \r 23
            \R 23
            \s 24
            \S 24
            \t 25
            \T 25
            \u 26
            \U 26
            \ú 27
            \Ú 27
            \ü 28
            \Ü 28
            \v 29
            \V 29
            \w 30
            \W 30
            \x 31
            \X 31
            \y 32
            \Y 32
            \z 33
            \Z 33
            \0 34
            \1 35
            \2 36
            \3 37
            \4 38
            \! 39
            \" 40
            \# 41
            \$ 42
            \% 43
            \& 44
            \' 45
            \( 46
            \) 47
            \* 48
            \+ 49
            \, 50
            \- 51
            \. 52
            \/ 53
            \: 54
            \; 55
            \< 56
            \= 57
            \> 58
            \? 59
            \@ 60
            \[ 61
            \\ 62
            \] 63
            \^ 64
            \_ 65
            \` 66
            \{ 67
            \| 68
            \} 69
            \~ 70
            \5 71
            \6 72
            \7 73
            \8 74
            \9 75))

(defn transposicion [m]
  (apply mapv vector m))

(defn t-cifrar
  [clave palabra]
  (let [tabla (tabla-transposicion)
        length (count clave)]
    (apply str (flatten (map (fn [m] (m :v)) (sort-by :i (apply vector (map (fn [li] (zipmap [:i :v] li)) (partition 2 (interleave (map (fn [x] (tabla x)) clave) (transposicion (partition length length (repeat length \space) palabra))))))))))))

;; KIBIT
;; (defn t-cifrar
;;   [clave palabra]
;;   (let [tabla (tabla-transposicion)
;;         length (count clave)]
;;     (clojure.string/join (flatten (map (fn [m] (m :v)) (sort-by :i (apply vector (map (fn [li] (zipmap [:i :v] li)) (partition 2 (interleave (map tabla clave) (transposicion (partition length length (repeat length \space) palabra))))))))))))

(defn t-descifrar
  [clave palabra]
  (let [tabla (tabla-transposicion)
        parte (quot (count palabra) (count clave))]
    (apply str (flatten (transposicion (map (fn [m] (m :v)) (sort-by :i (map (fn [li] (zipmap [:i :p :v] (list (first (first li)) (second (first li)) (second li)))) (partition 2 (interleave (sort-by second (partition 2 (interleave (range (count clave)) (map (fn [x] (tabla x)) clave)))) (partition parte parte (repeat parte \space) palabra)))))))))))

;; KIBIT
;; (defn t-descifrar
;;   [clave palabra]
;;   (let [tabla (tabla-transposicion)
;;         parte (quot (count palabra) (count clave))]
;;     (clojure.string/join (flatten (transposicion (map (fn [m] (m :v)) (sort-by :i (map (fn [li] (zipmap [:i :p :v] (list (ffirst li) (second (first li)) (second li)))) (partition 2 (interleave (sort-by second (partition 2 (interleave (range (count clave)) (map tabla clave)))) (partition parte parte (repeat parte \space) palabra)))))))))))