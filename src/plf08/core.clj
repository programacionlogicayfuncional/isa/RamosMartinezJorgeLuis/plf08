(ns plf08.core
  (:gen-class)
  (:require [plf08.polibio :as poli]
            [plf08.transposicion :as trans]))


;; PARÁMETROS
;; Tipo de cifrado (1=Polibio, 0=Transposicion)
;; Operación a realizar (1=Cifrado, 0=Descifrado)
;; Palabra clave (para el cifrado de Transposición)
;; Archivo de texto plano a leer
;; Archivo de texto plano a escribir

(defn -main
  [& args]
  (if (or (= (count args) 5) (= (count args) 4))
    (if (= (nth args 0) "1")
      (if (= (nth args 1) "1")
        (spit (nth args 3) (poli/p-cifrar (slurp (nth args 2))))
        (spit (nth args 3) (poli/p-descifrar (slurp (nth args 2)))))
      (if (= (nth args 1) "1")
        (spit (nth args 4) (trans/t-cifrar (nth args 2) (slurp (nth args 3))))
        (spit (nth args 4) (trans/t-descifrar (nth args 2) (slurp (nth args 3))))))
    (println "Error")))
