(ns plf08.core-test
  (:require [clojure.test :refer [deftest is testing]]
            [plf08.transposicion :refer [t-cifrar t-descifrar]]
            [plf08.polibio :refer [p-cifrar p-descifrar]]))


(deftest p-cifrar-test
  (testing "Cifrar la palabra programación"
    (is (= "♝♜♝♝♞♕♜♕♝♝♜♜♞♖♜♜♜♛♞♜♞♔♞♘" (p-cifrar "programación"))))
  (testing "Cifrar la palabra Clojure"
    (is (= "♗♗♞♚♞♕♞♝♝♖♝♝♜♖" (p-cifrar "Clojure"))))
  (testing "Cifrar la palabra Pingüino"
    (is (= "♔♞♞♜♞♘♜♕♝♗♞♜♞♘♞♕" (p-cifrar "Pingüino"))))
  (testing "Cifrar la palabra Polibio"
    (is (= "♔♞♞♕♞♚♞♜♜♝♞♜♞♕" (p-cifrar "Polibio"))))
  (testing "Cifrar la palabra Testing"
    (is (= "♔♖♜♖♝♛♝♚♞♜♞♘♜♕" (p-cifrar "Testing")))))

(deftest p-descifrar-test
  (testing "Descifrar la palabra Ingeniería"
    (is (= "Ingeniería" (p-descifrar "♕♛♞♘♜♕♜♖♞♘♞♜♜♖♝♝♞♞♜♜"))))
  (testing "Descifrar la palabra Sistemas"
    (is (= "Sistemas" (p-descifrar "♔♚♞♜♝♛♝♚♜♖♞♖♜♜♝♛"))))
  (testing "Descifrar %&#/$tlsrASFC"
    (is (= "%&#/$tlsrASFC" (p-descifrar "♚♝♚♛♚♜♖♝♚♞♝♚♞♚♝♛♝♝♗♖♔♚♕♜♗♗")))))

(deftest t-cifrar-test
  (testing "Cifrar una frase sin espacios"
    (is (= "HKSUTSILEYBE" (t-cifrar "CAT" "THESKYISBLUE")))
    (is (= "IOADDIMTHEOMAEUDROGSQAOD" (t-cifrar "GATO" "MIGATOSEHAQUEDADODORMIDO")))
    (is (= "NSÓLAM TSINURPRPCCMSLAOIONIE" (t-cifrar "FILA" "TRANSPOSICIÓNCOLUMNARSIMPLE"))))
  (testing "Cifrar una frase con espacios"
    (is (= "HS  UT YSLEKIBE" (t-cifrar "CAT" "THE SKY IS BLUE")))
    (is (= "ITE D M MASAEOROG HUDOD  O QADI " (t-cifrar "GATO" "MI GATO SE HA QUEDADO DORMIDO")))
    (is (= "NSÓONSL TSINLAIERPC URM AOICM P " (t-cifrar "FILA" "TRANSPOSICIÓN COLUMNAR SIMPLE"))))
  (testing "Cifrar una frase con la misma clave pero en minúsculas y mayúsculas"
    (is (= "HS  UT YSLEKIBE" (t-cifrar "Cat" "THE SKY IS BLUE")))
    (is (= "HS  UT YSLEKIBE" (t-cifrar "cAt" "THE SKY IS BLUE")))))

(deftest t-descifrar-test
  (testing "Descifrar una frase sin espacios"
    (is (= "THESKYISBLUE" (t-descifrar "CAT" "HKSUTSILEYBE")))
    (is (= "MIGATOSEHAQUEDADODORMIDO" (t-descifrar "GATO" "IOADDIMTHEOMAEUDROGSQAOD")))
    (is (= "TRANSPOSICIÓNCOLUMNARSIMPLE " (t-descifrar "FILA" "NSÓLAM TSINURPRPCCMSLAOIONIE"))))
  (testing "Descifrar una frase con espacios"
    (is (= "THE SKY IS BLUE" (t-descifrar "CAT" "HS  UT YSLEKIBE")))
    (is (= "MI GATO SE HA QUEDADO DORMIDO   " (t-descifrar "GATO" "ITE D M MASAEOROG HUDOD  O QADI ")))
    (is (= "TRANSPOSICIÓN COLUMNAR SIMPLE   " (t-descifrar "FILA" "NSÓONSL TSINLAIERPC URM AOICM P "))))
  (testing "Descifrar una frase con la misma clave pero en minúscula y mayúscula"
    (is (= "THE SKY IS BLUE" (t-descifrar "Cat" "HS  UT YSLEKIBE")))
    (is (= "THE SKY IS BLUE" (t-descifrar "cAt" "HS  UT YSLEKIBE")))))